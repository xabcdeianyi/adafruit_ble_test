import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ScanResult> adafruitScanResult = [];
  List<ScanResult> adafruitReadyConnect = [];
  List<bool> adafruitScanResultConnect = [];
  List<BluetoothCharacteristic> adafruitWriteCharacteristics = [];
  List<BluetoothCharacteristic> adafruitNotifyCharacteristics = [];
  List<String> rxString = [];

  String txMsg = "";
  List<int> bleMsg = [];

  @override
  Widget build(BuildContext context) {
    int receiveCallback(String localName) {
      for (var index = 0; index < adafruitReadyConnect.length; index++) {
        if (localName ==
            adafruitReadyConnect[index].advertisementData.localName) {
          return index;
        }
      }
      return 99;
    }

    Future<void> autoConnect() async {
      adafruitReadyConnect.addAll(adafruitScanResult);
      rxString.length = adafruitReadyConnect.length;
      for (var device in adafruitReadyConnect) {
        await device.device.connect();
        List<BluetoothService>? services =
        await device.device.discoverServices();
        for (BluetoothService s in services) {
          // Reads all characteristics
          var characteristics = s.characteristics;
          for (BluetoothCharacteristic c in characteristics) {
            if (c.uuid == Guid("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")) {
              adafruitNotifyCharacteristics.add(c);
              await c.setNotifyValue(true);
              c.value.listen((value) {
                int index = receiveCallback(device.advertisementData.localName);
                setState(() {
                  rxString[index] = String.fromCharCode(value[0]);
                });
              });

              // await c.write([]);
            } else if (c.uuid == Guid("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")) {
              adafruitWriteCharacteristics.add(c);
              // await c.write([58, 58, 58]);
            }
          }
        }
      }
    }

    Future<void> disconnectSingle(ScanResult scanResult) async {
      scanResult.device.disconnect();
      setState(() {
        int i = receiveCallback(scanResult.advertisementData.localName);
        scanResult.device.disconnect();
        adafruitScanResultConnect.removeAt(i);
        adafruitReadyConnect.removeAt(i);
        adafruitWriteCharacteristics.removeAt(i);
        adafruitNotifyCharacteristics[i].setNotifyValue(false);
        adafruitNotifyCharacteristics.removeAt(i);
        rxString.removeAt(i);
        adafruitScanResultConnect[i] = false;
      });
    }


    Future<void> connectSingle(ScanResult scanResult) async {
      // adafruitReadyConnect.addAll(adafruitScanResult);
      // rxString.length = adafruitReadyConnect.length;
      // for (var device in adafruitReadyConnect) {
      await scanResult.device.connect();
      adafruitScanResultConnect.add(true);
      adafruitReadyConnect.add(scanResult);
      scanResult.device.state.listen((event) async {
        if (event == BluetoothDeviceState.disconnected) {
          setState(() {
            int i = receiveCallback(scanResult.advertisementData.localName);
            scanResult.device.disconnect();
            adafruitScanResultConnect.removeAt(i);
            adafruitReadyConnect.removeAt(i);
            adafruitWriteCharacteristics.removeAt(i);
            adafruitNotifyCharacteristics[i].setNotifyValue(false);
            adafruitNotifyCharacteristics.removeAt(i);
            rxString.removeAt(i);
            adafruitScanResultConnect[i] = false;
          });
        }
      });
      List<BluetoothService>? services =
      await scanResult.device.discoverServices();
      for (BluetoothService s in services) {
        // Reads all characteristics
        var characteristics = s.characteristics;
        for (BluetoothCharacteristic c in characteristics) {
          if (c.uuid == Guid("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")) {
            adafruitNotifyCharacteristics.add(c);
            await c.setNotifyValue(true);
            rxString.add("");
            c.value.listen((value) {
              int index =
              receiveCallback(scanResult.advertisementData.localName);
              setState(() {
                rxString[index] = "";
                for (var text in value) {
                  rxString[index] = rxString[index] + String.fromCharCode(text);
                }
              });
            });

            // await c.write([]);
          } else if (c.uuid == Guid("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")) {
            adafruitWriteCharacteristics.add(c);
            // await c.write([58, 58, 58]);
          }
        }
      }
    }

    Widget textDisplay(ScanResult scanResult) {
      int index = receiveCallback(scanResult.advertisementData.localName);
      if (index == 99) {
        return const SizedBox();
      } else {
        try {
          return Text("Receive Value : ${rxString[index]}");
        } catch (e) {
          return const SizedBox();
        }
      }
    }

    Widget connectDisplay(ScanResult scanResult, int index) {
      int num = receiveCallback(scanResult.advertisementData.localName);
      if (num == 99) {
        return Container(
          width: 30,
          height: 30,
          decoration: const BoxDecoration(
            color: Colors.red,
            shape: BoxShape.circle,
          ),
          child: const Text(""),
        );
      } else {
        if (adafruitScanResultConnect[num] == true) {
          return Container(
            width: 30,
            height: 30,
            decoration: const BoxDecoration(
              color: Colors.green,
              shape: BoxShape.circle,
            ),
            child: const Text(""),
          );
        } else {
          return Container(
            width: 30,
            height: 30,
            decoration: const BoxDecoration(
              color: Colors.red,
              shape: BoxShape.circle,
            ),
            child: const Text(""),
          );
        }
      }
    }

    return Scaffold(
      body: Center(
        child: StreamBuilder<List<ScanResult>>(
          stream: FlutterBluePlus.instance.scanResults,
          builder: (context, snapshot) {
            try {
              adafruitScanResult =
                  snapshot.data!.where((e) => e.device.name != "").toList();
            } catch (e) {
              print(e);
            }

            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Press Device Name to Connect"),
                SizedBox(
                  height: 500,
                  width: 300,
                  child: ListView.builder(
                      itemCount: adafruitScanResult.length,
                      itemBuilder: (element, int index) {
                        return Center(
                          child: Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  // if (adafruitReadyConnect.contains(adafruitScanResult[index]) == true) {
                                  //   disconnectSingle(adafruitScanResult[index]);
                                  // } else {
                                  //   connectSingle(adafruitScanResult[index]);
                                  // }
                                  connectSingle(adafruitScanResult[index]);
                                },
                                child: connectDisplay(
                                    adafruitScanResult[index], index),
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(adafruitScanResult[index].device.name,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  // .substring(0, 8))),
                                  textDisplay(adafruitScanResult[index]),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 100,
                                        child: TextField(
                                          decoration: const InputDecoration(
                                              hintText: 'Input for bleWrite'),
                                          onChanged: (value) {
                                            txMsg = value;
                                          },
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          bleMsg = [];
                                          for (var i = 0;
                                          i < txMsg.length;
                                          i++) {
                                            bleMsg.add(0);
                                          }
                                          bleMsg = txMsg.codeUnits;
                                          adafruitWriteCharacteristics[
                                          receiveCallback(
                                              adafruitScanResult[index]
                                                  .advertisementData
                                                  .localName)]
                                              .write(bleMsg);
                                        },
                                        child: const Text("SEND"),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      }),
                ),
                // Text("${wtg10?.advertisementData.localName.toUpperCase()}",
                //     style: TextStyle(fontStyle: FontStyle.italic)),
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          for (var notify in adafruitNotifyCharacteristics) {
            notify.setNotifyValue(false);
          }
          for (var device in adafruitReadyConnect) {
            device.device.disconnect();
          }
          rxString = [];
          adafruitScanResultConnect = [];
          adafruitReadyConnect = [];
          adafruitNotifyCharacteristics = [];
          adafruitWriteCharacteristics = [];

          FlutterBluePlus.instance.startScan(timeout: const Duration(seconds: 3));

          // FlutterBlue.instance.stopScan();
        },
        child: const Text("scan"),
      ),
    );
  }
}
